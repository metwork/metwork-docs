# MetWork Documentation

Welcome to the documentation of [MetWork](https://metwork.pharmacie.parisdescartes.fr?ref=docs) !

MetWork is a web app that help you to anticipate your MS/MS annotation with a workflow based on simulation of bio-chemical transformations and _in-silico_ fragmentation.

!!! info "Overview"

    For a global overview of the app, you can have a look to [www.metwork.science](https://www.metwork.science?ref=docs)

This documentation is under construction and will be complete as things progress.

!!! note "Citation"

    If you use MetWork please cite :

    Yann Beauxis and Grégory Genta-Jouve
    Bioinformatics, Volume 35, Issue 10, 15 May 2019, Pages 1795–1796,
    [https://doi.org/10.1093/bioinformatics/bty864](https://doi.org/10.1093/bioinformatics/bty864 )
